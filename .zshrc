#!/bin/zsh

ZSH_CFG_PATH=".ah-my-zsh"
ZSH_CFG_FILE=".zshrc"
ZSH_SYNTAX_HIGHLIGHTING_FILE=~/$ZSH_CFG_PATH/syntax-highlighting/zsh-syntax-highlighting.zsh 

# Functions
function aliases() {
  cat ~/$ZSH_CFG_PATH/$ZSH_CFG_FILE |
	grep -P '^(?!aliases)alias' |
	sed s/alias//g
}

function show_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/'
}

# Init history
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# Format Prompt Prefix user@host:current_path
setopt prompt_subst
function show_prompt() {
    PS1="%n@%m:%~$(show_git_branch)$ "
}

precmd_functions+=(show_prompt)

# Completion
autoload -U compinit
compinit

# Zsh Syntax Highlighting
if [[ -e $ZSH_SYNTAX_HIGHLIGHTING_FILE ]]; then
   source $ZSH_SYNTAX_HIGHLIGHTING_FILE
fi

# Aliases
## ZSH related
alias changecfg="emacs -nw ~/$ZSH_CFG_PATH/$ZSH_CFG_FILE"
alias update="source ~/$ZSH_CFG_FILE"
## Shell
alias ls="ls --color=auto"
alias ll="ls -la"
alias j="cd"
alias b="cd .."
## Shows hidden files
alias l.="ls -d .* --color=auto"
## Git
alias ga="git add"
alias gall="git add ."
alias gcm="git checkout master"
alias gl="git pull"
alias gp="git push"
alias gs="git status"
alias gco="git checkout"
alias gt="git commit"
alias glog="git log"

# Init TMUX
if [[ -e /usr/bin/tmux && ! $TERM =~ screen ]]; then
    exec tmux
fi
